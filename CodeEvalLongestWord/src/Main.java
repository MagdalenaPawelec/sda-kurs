public class Main {

   
    public static void main(String[] args) {
    
       String line1 = "some line with text";
       String line2 = "another line";
       String line3 = "line with longest text inside";
       String line4 = "line with last longestlongestword";
       
       
       String result1 = findLongestWord(makeStringArray(line1));
       String result2 = findLongestWord(makeStringArray(line2));
       String result3 = findLongestWord(makeStringArray(line3));
       String result4 = findLongestWord(makeStringArray(line4));
       
       System.out.println(result1);
       System.out.println(result2);
       System.out.println(result3);
       System.out.println(result4);

       
        
    }
    
    private static String[] makeStringArray (String line){
        return line.split(" ");
    }
    
    private static String findLongestWord (String[] array){
        
        String longestString = array[0];
        
        for(int i = 0; i < array.length; i++){
            if((i+1) < array.length){
                if(array[i+1].length() > longestString.length()){
                    longestString = array[i+1];
                }
            }
        }
        return longestString;
    }
    
}