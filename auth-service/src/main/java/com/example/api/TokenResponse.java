package com.example.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TokenResponse {

    private final String token;

    public TokenResponse(@JsonProperty("token") String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
