package com.example.api;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LogoutRequest {


    private final String username;
    private final String token;

    @JsonCreator
    public LogoutRequest(@JsonProperty(value = "username", required = true) String username,@JsonProperty(value = "token", required = true) String token) {
        this.username = username;
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LogoutRequest{");
        sb.append("username='").append(username).append('\'');
        sb.append(", token='").append(token).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
