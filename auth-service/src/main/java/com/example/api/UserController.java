package com.example.api;

import com.example.model.AuthUser;
import com.example.service.TokenService;
import com.example.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.lang.invoke.MethodHandles;

@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final UserService userService;
    private final TokenService tokenService;

    @Autowired
    public UserController(UserService userService, TokenService tokenService) {
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @RequestMapping(path="/users/login", method = RequestMethod.POST)
    public TokenResponse login(@RequestBody LoginRequest loginRequest){
        AuthUser user = userService.getUser(loginRequest.getUsername(), loginRequest.getPassword());
        String token = tokenService.createToken(user);
        return new TokenResponse(token);
    }

    @RequestMapping(path="/users/logout", method = RequestMethod.PUT)
        public void logout(@RequestBody LogoutRequest logoutRequest){
        logger.info("Received request {}", logoutRequest);
        tokenService.deleteToken(logoutRequest.getToken(), logoutRequest.getUsername());

    }

}
