package com.example.model;


import com.google.common.base.MoreObjects;

public class AuthUser {

    private final String name;
    private final String password;

    public AuthUser(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .add("password", password)
                .toString();
    }
}
