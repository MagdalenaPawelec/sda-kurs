package com.example.service;

import com.example.model.AuthUser;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
public class TokenGenerator {
    private final SecureRandom secureRandom = new SecureRandom();

    public String createToken(AuthUser authUser){

        return authUser.getName() + secureRandom.nextInt();

    }

}
