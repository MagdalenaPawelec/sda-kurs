package com.example.service;

import com.example.model.AuthUser;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class UserService {

    private final ConcurrentHashMap<String, AuthUser> users = new ConcurrentHashMap<>();

    public UserService() {
        Lists.newArrayList(new AuthUser("Alice", "alice_pass"),
                new AuthUser("Bob", "bob_pass"),
                new AuthUser("Marcela", "marcel_pass"),
                new AuthUser("John", "john_pass"),
                new AuthUser("Charlie", "charlie_pass")
        ).stream().forEach(authUser -> users.put(authUser.getName(), authUser));

    }

    public AuthUser getUser(String username, String password) {
        Optional<AuthUser> authUserOpt = Optional.ofNullable(users.get(username));
        return authUserOpt
                .filter(authUser -> password.equals(authUser.getPassword()))
                .orElseThrow(() -> new UserNotFoundException(username));
    }

}
