package com.example.service;

import com.example.model.AuthUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TokenService {

    private final ConcurrentHashMap<String, AuthUser> tokenToUser = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<AuthUser,String> userToToken = new ConcurrentHashMap<>();
    private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    private final TokenGenerator tokenGenerator;

    @Autowired
    public TokenService(TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
    }

    public String createToken(AuthUser authUser) {
        logger.info("Creating token for user {}", authUser);

        Optional<String> userTokenOptional = Optional.ofNullable(userToToken.get(authUser));
        return userTokenOptional.orElseGet(() -> {
            String token = tokenGenerator.createToken(authUser);
            tokenToUser.put(token, authUser);
            userToToken.put(authUser, token);
            return token;
        });
    }

    public void deleteToken(String token, String username){

        Optional<AuthUser> authUserOptional = Optional.ofNullable(tokenToUser.get(token));
        Optional<AuthUser> existingUser = authUserOptional.filter(authUser -> authUser.getName().equals(username));

        if(existingUser.isPresent()){
            logger.info("Removing token for user {}", username);
            tokenToUser.remove(token);
            userToToken.remove(existingUser.get());
        } else {
            logger.info("User {} already has no token", username);
            throw new TokenNotFoundException(token);
        }
    }

}
