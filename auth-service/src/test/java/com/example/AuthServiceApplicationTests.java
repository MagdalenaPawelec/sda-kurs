package com.example;

import com.example.api.LoginRequest;
import com.example.api.LogoutRequest;
import com.example.api.TokenResponse;
import com.googlecode.catchexception.CatchException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AuthServiceApplication.class)
@WebIntegrationTest(randomPort = true)
//@WebAppConfiguration
public class AuthServiceApplicationTests {

	private static final String LOGIN_URL = "http://localhost:%d/users/login";
	private static final String LOGOUT_URL = "http://localhost:%d/users/logout";

	@Value("${local.server.port}")
	private int port;

	private RestTemplate restTemplate = new RestTemplate();

	@Test
	public void shouldLoginUser() {
		//given
		LoginRequest loginRequest = new LoginRequest("Alice", "alice_pass");


		//when
		ResponseEntity<TokenResponse> response = restTemplate.postForEntity(urlWithPort(LOGIN_URL), loginRequest, TokenResponse.class);

		//then

		Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
		Assertions.assertThat(response.getBody().getToken()).isEqualTo("Alicealice_pass");

	}

	@Test
	public void shouldLoginAndLogOut(){
		//given
		LoginRequest loginRequest = new LoginRequest("Alice", "alice_pass");


		//when
		TokenResponse tokenResponse = restTemplate.postForEntity(urlWithPort(LOGIN_URL), loginRequest, TokenResponse.class).getBody();
		LogoutRequest logoutRequest = new LogoutRequest(loginRequest.getUsername(), tokenResponse.getToken());
		restTemplate.put(urlWithPort(LOGOUT_URL), logoutRequest);

		//then ok

	}

	@Test
	public void shouldReturnHttp404WhenUserIsNotLoggedIn(){
		//given
		LogoutRequest logoutRequest = new LogoutRequest("Alice", "Alicealice_pass");

		//when
		catchException(restTemplate).put(urlWithPort(LOGOUT_URL), logoutRequest);

		//then
		HttpClientErrorException e = CatchException.caughtException();
		Assertions.assertThat(e.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

	}

	@Test
	public void shouldReturnHttp404WhenUsernameDoesNotMatch (){
		//given
		LoginRequest loginRequest = new LoginRequest("Alice", "alice_pass");

		//when
		TokenResponse tokenResponse = restTemplate.postForObject(urlWithPort(LOGIN_URL), loginRequest, TokenResponse.class);
		LogoutRequest logoutRequest = new LogoutRequest("ZleImie", tokenResponse.getToken());

		catchException(restTemplate).put(urlWithPort(LOGOUT_URL), logoutRequest);

		//then
		HttpClientErrorException e = CatchException.caughtException();
		Assertions.assertThat(e.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);


	}

	private String urlWithPort(String template){
		return String.format(template, port);
	}

}
