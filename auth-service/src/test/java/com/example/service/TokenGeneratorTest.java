package com.example.service;

import com.example.model.AuthUser;
import com.example.service.TokenGenerator;
import org.assertj.core.api.Assertions;
import org.junit.Test;


public class TokenGeneratorTest {
    private TokenGenerator tokenGenerator = new TokenGenerator();

    @Test
    public void shouldAlwaysGenerateNewToken(){
        //given
        AuthUser authUser = new AuthUser("Alice", "password");

        //when

        String token1 = tokenGenerator.createToken(authUser);
        String token2 = tokenGenerator.createToken(authUser);

        //then
        Assertions.assertThat(token1).isNotEqualTo(token2);
    }
}