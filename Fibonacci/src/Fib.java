import java.util.Scanner;

public class Fib {
	public static void main(String[] args) {

		int maxElems = 15;

		int[] value = new int[maxElems];
		value[0] = 0;
		value[1] = 1;
		System.out.println(value[0]);
		System.out.println(value[1]);

		for (int i = 2; i < maxElems; i++) {

			System.out.println(value[i] = value[i - 2] + value[i - 1]);
		}
		
		
		
		System.out.println("-----------------");

		
		
		System.out.println(findFibonacci(12));

	}

	private static int findFibonacci(int fn) {

		int f0 = 0;
		int f1 = 1;
		int result = 0;

		for (int i = 1; i < fn; i++) {
			result = f0 + f1;

			int temp = result;
			f0 = f1;
			f1 = temp;
		}
		return result;

	}
}