
public class FriesWorker extends Worker{

	
	public FriesWorker(String name, String surname, String typeOfWorker, int timeout) {
		super(name, surname, typeOfWorker, timeout);
		
	}

	@Override
	public int getSpeed() {
	
		return 10;
	}
	

	@Override
	public void run() {
		int loopCounter = 0;
		while (loopCounter < 3){
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			McDonald.getInstance().increaseFriesCounter(name);
			
			loopCounter++;
		}
		
	}

}
