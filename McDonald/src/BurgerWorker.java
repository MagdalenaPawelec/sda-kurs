
public class BurgerWorker extends Worker{

	public BurgerWorker(String name, String surname, String typeOfWorker, int timeout) {
		super(name, surname, typeOfWorker, timeout);
		
	}

	@Override
	public int getSpeed() {
		return 20;
	}

	@Override
	public void run() {
		int loopCounter = 0;
		while (loopCounter < 3){
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			McDonald.getInstance().increaseBurgerCounter(name);
			loopCounter++;
		}
		
	}

}
