import java.util.HashMap;


public class McDonald {
	public static McDonald instance;

	
	public Integer burgerCounter = null;
	public Integer friesCounter = null;
	private HashMap<String, String> myHashMap = null;
	
	private McDonald(){
		burgerCounter = 0;
		friesCounter = 0;
		myHashMap = new HashMap<>();
	}
	
	public synchronized static McDonald getInstance(){
		if(instance == null){		
			instance = new McDonald();
		}
		return instance;
	}

	
	public synchronized void increaseBurgerCounter(String who){
		burgerCounter++;
		System.out.println("Thread " + who + " increasing to " + burgerCounter);

		try{
			Thread.sleep(500);
		} catch (Exception e){
			
		}
	}
	
	public synchronized void increaseFriesCounter(String who){
		friesCounter++;
		System.out.println("Thread " + who + " increasing to " + friesCounter);

		try{
			Thread.sleep(500);
		} catch (Exception e){
			
		}
	}
	
	public int getBurgerCounter(){
		return burgerCounter;
	}
	
	public int getFriesCounter(){
		return friesCounter;
	}

	public void setBurgerCounter(Integer burgerCounter) {
		this.burgerCounter = burgerCounter;
	}

	public void setFriesCounter(Integer friesCounter) {
		this.friesCounter = friesCounter;
	}
	
}
