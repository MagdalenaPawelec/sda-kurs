

public class Main {

	public static void main(String[] args) {
		
		Worker worker1 = FactoryWorker.createBurgerWorker();
		System.out.println("type of worker: " + worker1.getTypeOfWorker() + "; worker's speed: "+ worker1.getSpeed() + "; name of worker: " + worker1.getName() + " " + worker1.getSurname());

		Worker worker2 = FactoryWorker.createBurgerWorker();
		System.out.println("type of worker: " + worker2.getTypeOfWorker() + "; worker's speed: "+ worker2.getSpeed() + "; name of worker: " + worker2.getName() + " " + worker2.getSurname());
		
		Worker worker3 = FactoryWorker.createFriesWorker();
		System.out.println("type of worker: " + worker3.getTypeOfWorker() + "; worker's speed: "+ worker3.getSpeed() + "; name of worker: " + worker3.getName() + " " + worker3.getSurname());
		

		
		
		Thread thread1 = new Thread(worker1);
		Thread thread2 = new Thread(worker2);
		Thread thread3 = new Thread(worker3);
		
		thread1.start();
		thread2.start();
		thread3.start();
		
	}

}
