
public class FactoryWorker {
	private static int burgerWorkerCounter = 1;
	private static int friesWorkerCounter = 1;
	
	public static BurgerWorker createBurgerWorker(){
		return new BurgerWorker("Cheese" + burgerWorkerCounter++, "Burger" + burgerWorkerCounter, "BurgerWorker", 300);
	}
	
	public static FriesWorker createFriesWorker(){
		return new FriesWorker("Fryt" + friesWorkerCounter++, "Frytek" + friesWorkerCounter, "FriesWorker", 100);
	}
}
