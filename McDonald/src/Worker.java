
public abstract class Worker implements Runnable {
	protected String name;
	private String surname;
	private String typeOfWorker;
	protected int timeout;
	
	
	public Worker(String name, String surname, String typeOfWorker, int timeout){
		this.name = name;
		this.surname = surname;
		this.typeOfWorker = typeOfWorker;
		this.timeout = timeout;
	}	
	
	public String getTypeOfWorker(){
		return typeOfWorker;
	}
	
	public abstract int getSpeed();

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}
	
	
}






	
	

	
