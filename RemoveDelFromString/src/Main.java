public class Main {

    public static final Character CHAR_D = 'd';
    public static final Character CHAR_E = 'e';
    public static final Character CHAR_L = 'l';

    public static void main(String[] args) {
        Main main = new Main();
        System.out.println(main.delDel("adel"));
        System.out.println(main.delDel("adeld"));
        System.out.println(main.delDel("del"));
        System.out.println(main.delDel("assdeldeee"));
    }

    public String delDel(String str) {
        int length = str.length();
        for(int i=0; i < length; i++){
            if(i+2 < length && CHAR_D.equals(str.charAt(i)) && CHAR_E.equals(str.charAt(i+1)) && CHAR_L.equals(str.charAt(i+2))){
                return (str.substring(0, i) + str.substring(i+3,length));
            }
        }
        return str;
    }

}